#include <stdio.h>
#define __USE_MISC 1
#include "mem_internals.h"
#include "mem.h"
#define PASS_FORMAT "Test %d passed"
#define FAIL_FORMAT "Test %d failed"
#define HEAP_SIZE capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes
#define run_test(i) \
    if (!test##i()) { \
        printf(FAIL_FORMAT, i); \
        return 1; \
    } else { \
        printf(PASS_FORMAT, i); \
    }

static struct block_header* get_header(void* content) {
    return (struct block_header*) (content - offsetof(struct block_header, contents));
}

// ----- TEST 1 -----
// Обычное успешное выделение памяти.
bool test1() {
    // Инициализируем кучу
    void* heap = heap_init(HEAP_SIZE);
    void* block = _malloc(512);

    if (block == NULL || heap == NULL) {
        return false;
    }

    _free(block);
    munmap(heap, HEAP_SIZE);

    return true;
}

// ----- TEST 2 -----
// Освобождение одного блока из нескольких выделенных.
bool test2() {
    void* heap = heap_init(HEAP_SIZE);
    void* block1 = _malloc(100);
    void* block2 = _malloc(100);
    void* block3 = _malloc(100);
    struct block_header *h3 = get_header(block3);

    if (heap == NULL || block1 == NULL || block2 == NULL || block3 == NULL) {
        return false;
    }   

    // Должен произойти мердж после free третьего блока
    size_t next_size = size_from_capacity(h3->next->capacity).bytes;
    _free(block3);

    if (!h3->is_free || h3->capacity.bytes != next_size + 100) {
        return false;
    }

    _free(block1);
    _free(block2);
    munmap(heap, HEAP_SIZE);

    return true;
}

// ----- TEST 3 -----
// Освобождение двух блоков из нескольких выделенных.
bool test3() {
    void* heap = heap_init(HEAP_SIZE);
    void* block1 = _malloc(100);
    void* block2 = _malloc(100);
    void* block3 = _malloc(100);
    void *block4 = _malloc(100);
    struct block_header *h2 = get_header(block2);
    struct block_header *h3 = get_header(block3);
    struct block_header *h4 = get_header(block4);

    if (block1 == NULL || block2 == NULL || block3 == NULL || block4 == NULL) {
        return false;
    }

    size_t b3size = size_from_capacity(h3->capacity).bytes;
    // В обратном порядке, чтоб произошел мердж
    _free(block3);
    _free(block2);

    if (!h2->is_free || h2->next != h4 || h2->capacity.bytes != 100 + b3size) {
        return false;
    }

    _free(block1);
    _free(block4);
    munmap(heap, HEAP_SIZE);

    return true;
}

// ----- TEST 4 -----
// Память закончилась, новый регион памяти расширяет старый.
bool test4() {
    void* heap = heap_init(HEAP_SIZE);
    // Должен занять весь регион
    void* block1 = _malloc(REGION_MIN_SIZE);
    struct block_header *h1 = get_header(block1);

    if (heap == NULL || block1 == NULL || h1->capacity.bytes != REGION_MIN_SIZE || h1->next != NULL) {
        return false;
    }

    void* block2 = _malloc(100);
    struct block_header *h2 = get_header(block1);

    if (block2 != h1->contents + REGION_MIN_SIZE + offsetof(struct block_header, contents) || h1->next != h2) {
        return false;
    }

    // В обатном порядке, чтобы произошёл мердж и мы оба региона очистили
    _free(block2);
    _free(block1);
    munmap(heap, size_from_capacity(h1->capacity).bytes);
    
    return true;
}

// ----- TEST 5 -----
// Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион
bool test5() {
    void* heap = heap_init(HEAP_SIZE);
    
    void* block1 = _malloc(REGION_MIN_SIZE);
    struct block_header *h1 = get_header(block1);

    if (heap == NULL || block1 == NULL || h1->capacity.bytes != REGION_MIN_SIZE || h1->next != NULL) {
        return 0;
    }

    debug_heap(stdout, heap);

    // Вручную занять последующий диапозон адресов чтобы мы не смогли расширять старый
    void* p = mmap(h1->contents + h1->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    if (p == MAP_FAILED) {
        return false;
    }

    void *block2 = _malloc(REGION_MIN_SIZE);
    struct block_header *h2 = get_header(block2);

    if (block2 == NULL || h2 == (void*) (h1->contents + h1->capacity.bytes)) {
        return false;
    }

    _free(block1);
    _free(block2);
    munmap(heap, size_from_capacity(h1->capacity).bytes + REGION_MIN_SIZE + size_from_capacity(h2->capacity).bytes);

    return true;
}

int main() {
    run_test(1);
    run_test(2);
    run_test(3);
    run_test(4);
    run_test(5);

    return 0;
}
